using UnityEngine;
using System;
using System.Threading;
using System.Globalization;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Reflection;
using Newtonsoft.Json;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;

namespace automation.config
{
	public static class AutomationConfig {
		
		public static JToken GetAutomationConfig() {
			JToken json = ReadClipboard();
			if (json == null)
				json = new JObject();
			if (json["automation"] != null)
				json = json["automation"];
			return json;
		}

		#if UNITY_IOS
		[DllImport("__Internal")]
		static extern string GetText_();
		#endif

		private static JObject ReadClipboard() {
			JObject json = null;

			string text = "";
			#if UNITY_EDITOR
			text = GUIUtility.systemCopyBuffer;
			#elif UNITY_ANDROID
			using (var unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
			using (var context = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"))
			using (var clipboardManager = context.Call<AndroidJavaObject>("getSystemService", "clipboard"))
			using (var primaryClip = clipboardManager.Call<AndroidJavaObject>("getPrimaryClip"))
			using (var clipDataItem = primaryClip.Call<AndroidJavaObject>("getItemAt", 0)) 
			{
				if (clipDataItem != null) {
					text = clipDataItem.Call<string>("getText");
				}
			}
			#elif UNITY_IOS
			text = GetText_();
			#endif

			try {
				json = JsonConvert.DeserializeObject<JObject>(text);
			}
			catch (System.Exception e) {
				Debug.LogError("Invalid automation JSON");
			}

			return json;
		}
	}
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using altSocket.utils;

namespace altConfig.events{

public class ConnectConfigCommand : UnityEvent<string, AltClientSocketHandler> { }
public class UnknownStringCommand : UnityEvent<AltClientSocketHandler> { }
public class CloseConnectionCommand : UnityEvent<AltClientSocketHandler> { }
public class ReadConfigCommand : UnityEvent<AltClientSocketHandler> { }
public class ResetConfigCommand : UnityEvent<AltClientSocketHandler> { }
public class UpdateConfigCommand : UnityEvent<string, string, AltClientSocketHandler> { }
public class ReadMetricsCommand : UnityEvent<AltClientSocketHandler> { }
public class PingCommand : UnityEvent<AltClientSocketHandler> { }

public class CompanionServiceEvents {

    public UnityEvent ResponseReceived;
    public CloseConnectionCommand CloseConnection;
    public UnknownStringCommand UnknownString;
	public ConnectConfigCommand ConnectConfigServer;
	public ReadConfigCommand ReadConfig;
	public ResetConfigCommand ResetConfig;
	public UpdateConfigCommand UpdateConfig;
	public ReadMetricsCommand ReadMetrics;
	public PingCommand Ping;
    // We are a singleton!
    private static CompanionServiceEvents _instance;
    public static CompanionServiceEvents Instance {
        get {
            if (_instance == null) {
                _instance = new CompanionServiceEvents();
                _instance.UnknownString = new UnknownStringCommand();
                _instance.ResponseReceived = new UnityEvent();
                _instance.CloseConnection = new CloseConnectionCommand();
				_instance.ConnectConfigServer = new ConnectConfigCommand ();
				_instance.ReadConfig = new ReadConfigCommand ();
				_instance.ResetConfig = new ResetConfigCommand ();
				_instance.UpdateConfig = new UpdateConfigCommand ();
				_instance.ReadMetrics = new ReadMetricsCommand ();
				_instance.Ping = new PingCommand ();
            }
            return _instance;
        }
    }
}
}
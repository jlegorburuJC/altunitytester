﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using altConfig.events;
// using altSocket.utils;
using automation.config;
using gs.data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using altSocket.utils;

public class CompanionServiceDriver : MonoBehaviour, AltIClientSocketHandlerDelegate {

	private const string CONNECT_MSG = "{\"id\":\"connect\", \"protocolVersion\": \"1.0\", \"ip\": \"127.0.0.1\" ,\"port\": \"%PORT%\", \"gameId\": \"%GAMEID%\", \"signature\":\"companion\"}";
	private const string CONFIG_MSG = "{\"id\": \"%IDMESSAGE%\"}";
	private const string UPDATE_MSG = "{\"id\": \"update\", \"modifyConfig\": %MODIFCONFIG%, \"overlay\": %OVERLAY%}";
	private AltSocketServer socketServer;

	private readonly string errorNotConnectedMessage = "error:notConnected";
	private readonly string errorCouldNotPerformOperationMessage = "error:couldNotPerformOperation";
	private readonly string errorConfigServerNotConnected = "error:configServerNotConnected";

	private JsonSerializerSettings jsonSettings;

	public int socketPortNumber = 15000;
	private Socket configSocket;
	public bool debugBuildNeeded = false;

	private static AltResponseQueue responseQueue;

	void Start () {
		//JToken config = AutomationConfig.GetAutomationConfig ();
		//if (config["enable"] == null) {
		//	Debug.LogError ("automation config is not enabled");
		//	return;
		//}
		jsonSettings = new JsonSerializerSettings ();
		jsonSettings.NullValueHandling = NullValueHandling.Ignore;

		responseQueue = new AltResponseQueue ();

		CompanionServiceEvents.Instance.CloseConnection.AddListener (CloseConnection);
		CompanionServiceEvents.Instance.UnknownString.AddListener (UnknownString);
		CompanionServiceEvents.Instance.ConnectConfigServer.AddListener (ConnectConfigServer);
		CompanionServiceEvents.Instance.ReadConfig.AddListener (ReadConfig);
		CompanionServiceEvents.Instance.UpdateConfig.AddListener (UpdateConfig);
		CompanionServiceEvents.Instance.ResetConfig.AddListener (ResetConfig);
		CompanionServiceEvents.Instance.ReadMetrics.AddListener (ReadMetrics);
		CompanionServiceEvents.Instance.Ping.AddListener (Ping);

		if (debugBuildNeeded && !Debug.isDebugBuild) {
			Debug.Log ("CompanionService will not run if this is not a Debug/Development build");
		} else {
			DontDestroyOnLoad (this);
			StartSocketServer ();
			Debug.Log ("CompanionService Driver started");
		}
	}
	public string StartConfigServer (string gameID) {
		try {
			//Broadcast Socket
			Socket s = new Socket (AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			s.SetSocketOption (SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
			IPEndPoint broadcastEndPoint = new IPEndPoint (IPAddress.Any, 0);
			s.Bind (broadcastEndPoint);
			s.SetSocketOption (SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
			s.SetSocketOption (SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 255);
			s.Blocking = false;
			//Server Socket for GS to connect
			Socket server = new Socket (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			IPEndPoint serverEndPoint = new IPEndPoint (IPAddress.Parse ("127.0.0.1"), 0);
			server.Bind (serverEndPoint);
			server.Listen (1);
			string data = CONNECT_MSG.Replace ("%PORT%", ((IPEndPoint) server.LocalEndPoint).Port.ToString ()).Replace ("%GAMEID%", gameID);
			byte[] msg = FormatMessage (data);
			IPEndPoint configEndPoint = new IPEndPoint (IPAddress.Parse ("127.0.0.1"), 53474);
			s.SendTo (msg, configEndPoint);
			this.configSocket = server.Accept ();
			s.Close ();
			server.Close ();
			return "Connected";
		} catch (Exception) {
			return errorNotConnectedMessage;
		}
	}

	public void CloseConfigServer () {
		this.configSocket.Close ();
	}

	public string ConfigMessage (string idMessage) {
		string data = CONFIG_MSG.Replace ("%IDMESSAGE%", idMessage);
		return SendAndReadMessage (data);
	}

	public string UpdateConfig (bool modifConfig, string config) {
		var delta = JSON.Parse<IList<object>> (config);
		var overlay = Data.ApplyDelta (Data.Obj (), delta);
		string data = UPDATE_MSG.Replace ("%MODIFCONFIG%", modifConfig.ToString ().ToLower ()).Replace ("%OVERLAY%", JSON.Stringify (overlay));
		try {
			SendConfigMessage (data);
			return "Update Config sent";
		} catch (Exception) {
			return errorConfigServerNotConnected;
		}
	}

	private string SendAndReadMessage (string data) {
		try {
			SendConfigMessage (data);
			return ReadMessage ();
		} catch (Exception) {
			return errorConfigServerNotConnected;
		}
	}

	private void SendConfigMessage (string data) {
		byte[] msg = FormatMessage (data);
		configSocket.Send (msg);
	}

	public byte[] FormatMessage (string data) {
		byte[] msg_bytes = Encoding.UTF8.GetBytes (data);
		byte[] size_bytes = BitConverter.GetBytes (data.Length);
		if (BitConverter.IsLittleEndian)
			Array.Reverse (size_bytes);
		List<byte> size = new List<byte> (size_bytes);
		List<byte> msg = new List<byte> (msg_bytes);
		size.AddRange (msg);
		return size.ToArray ();
	}

	private string ReadMessage () {
		// Get message size
		byte[] msg_size = new Byte[4];
		this.configSocket.Receive (msg_size, 4, SocketFlags.None);
		if (BitConverter.IsLittleEndian)
			Array.Reverse (msg_size);
		int size = BitConverter.ToInt32 (msg_size, 0);
		Debug.Log ("Message size:" + size);
		// Read message
		byte[] response_msg = new Byte[size];
		string final_msg = "";
		int totalRecv = 0;
		while (totalRecv < size) {
			int recv = this.configSocket.Receive (response_msg, size - totalRecv, SocketFlags.None);
			Debug.Log ("Size received: " + recv);
			totalRecv += recv;
			string result_msg = System.Text.Encoding.UTF8.GetString (response_msg, 0, recv);
			final_msg += result_msg;
		}
		return (final_msg);
	}

	public void StartSocketServer () {
		AltIClientSocketHandlerDelegate clientSocketHandlerDelegate = this;
		int maxClients = 1;
		string separatorString = "&";
		Encoding encoding = Encoding.UTF8;

		this.socketServer = new AltSocketServer (
			clientSocketHandlerDelegate, socketPortNumber, maxClients, separatorString, encoding);

		this.socketServer.StartListeningForConnections ();

		Debug.Log (String.Format (
			"CompanionService Server at {0} on port {1}",
			this.socketServer.LocalEndPoint.Address, this.socketServer.PortNumber));
	}

	void OnApplicationQuit () {
		CleanUp ();
	}

	public void CleanUp () {
		Debug.Log ("Cleaning up socket server");
		this.socketServer.Cleanup ();
	}

	public void ClientSocketHandlerDidReadMessage (AltClientSocketHandler handler, string message) {
		string[] pieces = message.Split (';');

		switch (pieces[0]) {
		case "connect":
			Debug.Log ("Starting Config Server for: " + pieces[1]);
			CompanionServiceEvents.Instance.ConnectConfigServer.Invoke (pieces[1], handler);
			break;
		case "readConfig":
			Debug.Log ("Reading configuration");
			CompanionServiceEvents.Instance.ReadConfig.Invoke (handler);
			break;
		case "resetConfig":
			Debug.Log ("Reseting configuration");
			CompanionServiceEvents.Instance.ResetConfig.Invoke (handler);
			break;
		case "updateConfig":
			Debug.Log ("Updating configuration");
			CompanionServiceEvents.Instance.UpdateConfig.Invoke (pieces[1], pieces[2], handler);
			break;
		case "readMetrics":
			Debug.Log ("Reading metrics");
			CompanionServiceEvents.Instance.ReadMetrics.Invoke (handler);
			break;
		case "ping":
			Debug.Log ("Config Ping received");
			CompanionServiceEvents.Instance.Ping.Invoke (handler);
			break;
		case "closeConnection":
			Debug.Log ("Socket connection closed!");
			CompanionServiceEvents.Instance.CloseConnection.Invoke (handler);
			break;
		default:
			CompanionServiceEvents.Instance.UnknownString.Invoke (handler);
			break;
		}
	}

	private void ConnectConfigServer (string gameID, AltClientSocketHandler handler) {
		responseQueue.ScheduleResponse (delegate {
			new Thread (() => {
				Dictionary<string, string> result = new Dictionary<string, string> () { { "result", StartConfigServer (gameID) }
				};
				string response = JsonConvert.SerializeObject (result);
				handler.SendResponse (response);
			}).Start ();
		});
	}

	private void ReadConfig (AltClientSocketHandler handler) {
		responseQueue.ScheduleResponse (delegate {
			new Thread (() => {
				string response = JsonConvert.SerializeObject (ConfigMessage ("readConfiguration"));
				handler.SendResponse (response);
			}).Start ();
		});
	}

	private void ResetConfig (AltClientSocketHandler handler) {
		responseQueue.ScheduleResponse (delegate {
			new Thread (() => {
				string response = JsonConvert.SerializeObject (ConfigMessage ("reset"));
				handler.SendResponse (response);
			}).Start ();
		});
	}

	private void UpdateConfig (string modifConfig, string overlay, AltClientSocketHandler handler) {
		responseQueue.ScheduleResponse (delegate {
			new Thread (() => {
				string response = JsonConvert.SerializeObject (UpdateConfig (bool.Parse (modifConfig), overlay));
				handler.SendResponse (response);
			}).Start ();
		});
	}

	private void ReadMetrics (AltClientSocketHandler handler) {
		responseQueue.ScheduleResponse (delegate {
			new Thread (() => {
				string response = JsonConvert.SerializeObject (ConfigMessage ("readMetrics"));
				handler.SendResponse (response);
			}).Start ();
		});
	}

	private void Ping (AltClientSocketHandler handler) {
		responseQueue.ScheduleResponse (delegate {
			new Thread (() => {
				string response = JsonConvert.SerializeObject (ConfigMessage ("ping"));
				handler.SendResponse (response);
			}).Start ();
		});
	}

	private void CloseConnection (AltClientSocketHandler handler) {
		Debug.Log ("Closing CompanionService Server");
		CloseConfigServer ();
		socketServer.StopListeningForConnections ();
	}

	private void UnknownString (AltClientSocketHandler handler) {
		responseQueue.ScheduleResponse (delegate {
			handler.SendResponse (errorCouldNotPerformOperationMessage);
		});
	}

	void Update () {
		if (responseQueue != null)
			responseQueue.Cycle ();
	}
}